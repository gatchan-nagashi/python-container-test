FROM public.ecr.aws/lambda/python:3.8 as builder

# Copy function code
COPY app.py ${LAMBDA_TASK_ROOT}

# Install the function's dependencies using file requirements.txt
# from your project folder.

COPY requirements.txt  .
# COPY yolov4.weights ${LAMBDA_TASK_ROOT}

RUN yum install gcc-c++ -y

RUN  pip install --upgrade pip
RUN  pip install numpy
RUN  pip install numpy --target "${LAMBDA_TASK_ROOT}"
RUN  pip install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

# Multi Stage
FROM public.ecr.aws/lambda/python:3.8

COPY --from=builder ${LAMBDA_TASK_ROOT} ${LAMBDA_TASK_ROOT}
# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "app.handler" ] 
